cargo +nightly build -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target x86_64-pc-windows-gnu --release
copy target\x86_64-pc-windows-gnu\release\mixtape.exe .
upx --best --lzma mixtape.exe
