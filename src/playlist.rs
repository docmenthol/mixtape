use std::fmt::Debug;
use pest::Parser;
use pest_derive::Parser;
use std::io;
use pest::iterators::Pair;

#[derive(Parser)]
#[grammar = "playlist.pest"]
struct PlaylistParser;

#[derive(Default)]
pub struct Note {
    start: f32,
    end: f32,
    text: String,
}

#[derive(Default)]
pub struct Song {
    filename: String,
    notes: Vec<Note>,
}

#[derive(Default)]
pub struct Playlist {
    from: String,
    to: String,
    songs: Vec<Song>,
    filename: String
}

impl Debug for Playlist {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Playlist")
            .field("from", &self.from)
            .field("to", &self.to)
            .field("songs", &self.songs.len())
            .finish()
    }
}

impl Debug for Song {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Song")
            .field("filename", &self.filename)
            .field("notes", &self.notes.len())
            .finish()
    }
}

fn parse_note(note: Pair<Rule>) -> Note {
    let mut n = Note::default();
    for line in note.into_inner() {
        match line.as_rule() {
            Rule::start => {
                n.start = line.as_str().parse().unwrap();
            }
            Rule::end => {
                n.end = line.as_str().parse().unwrap();
            }
            Rule::words => {
                n.text = line.to_string();
            }
            _ => {}
        }
    }

    n
}

fn parse_song(song: Pair<Rule>) -> Song {
    let mut s = Song::default();
    for line in song.into_inner() {
        match line.as_rule() {
            Rule::filename => {
                s.filename = line.to_string();
            }
            Rule::note => {
                let mut inner_rules = line.into_inner();
                s.notes.push(parse_note(inner_rules.next().unwrap()));
            }
            _ => {}
        }
    }

    s
}

pub fn parse_playlist_file(filename: &str) -> io::Result<Playlist> {
    let contents = std::fs::read_to_string(filename)?;
    let mut file = PlaylistParser::parse(
        Rule::playlist,
        &contents
    ).unwrap();

    let mut p = Playlist::default();
    p.filename = filename.to_string();

    for line in file.next().unwrap().into_inner() {
        match line.as_rule() {
            Rule::from => {
                let mut inner_rules = line.into_inner();
                p.from = inner_rules.next().unwrap().as_str().to_string();
            }
            Rule::to => {
                let mut inner_rules = line.into_inner();
                p.to = inner_rules.next().unwrap().as_str().to_string();
            }
            Rule::song => {
                let mut inner_rules = line.into_inner();
                p.songs.push(parse_song(inner_rules.next().unwrap()));
            }
            _ => {}
        }
    }

    Ok(p)
}

#[cfg(test)]
mod tests {
    #[test]
    fn loads_playlists() {
        let p = super::parse_playlist_file("resources/test_playlist.mix").unwrap();
        assert_eq!(p.from, "slippery");
        assert_eq!(p.to, "shine");
        assert_eq!(p.songs.len(), 3);
    }
}