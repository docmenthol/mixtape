use std::cell::RefCell;
use nwd::NwgUi;
use nwg::NativeUi;
use winapi::um::wincon::{AttachConsole, ATTACH_PARENT_PROCESS};

use crate::state::State;

const ICON: &[u8] = include_bytes!("../resources/icon.ico");

#[derive(Default, NwgUi)]
pub struct WindowsApp {
    state: RefCell<State>,

    #[nwg_control(size: (300, 115), position: (300, 300), title: "mixtape", flags: "WINDOW|VISIBLE")]
    #[nwg_events( OnWindowClose: [WindowsApp::event_close] )]
    window: nwg::Window,

    #[nwg_resource(source_bin: Some(ICON))]
    icon: nwg::Icon,

    #[nwg_layout(parent: window, spacing: 1)]
    grid: nwg::GridLayout,

    #[nwg_control(text: "mixtape", focus: true)]
    #[nwg_layout_item(layout: grid, col: 0, row: 0, col_span: 4)]
    name_edit: nwg::TextInput,

    #[nwg_control(text: "Prev")]
    #[nwg_layout_item(layout: grid, col: 0, row: 1)]
    #[nwg_events( OnButtonClick: [WindowsApp::event_previous_track] )]
    previous_track_button: nwg::Button,

    #[nwg_control(text: "P/P")]
    #[nwg_layout_item(layout: grid, col: 1, row: 1)]
    #[nwg_events( OnButtonClick: [WindowsApp::event_play_pause] )]
    play_pause_button: nwg::Button,

    #[nwg_control(text: "Stop")]
    #[nwg_layout_item(layout: grid, col: 2, row: 1)]
    #[nwg_events( OnButtonClick: [WindowsApp::event_stop] )]
    stop_button: nwg::Button,

    #[nwg_control(text: "Next")]
    #[nwg_layout_item(layout: grid, col: 3, row: 1)]
    #[nwg_events( OnButtonClick: [WindowsApp::event_next_track] )]
    next_track_button: nwg::Button,
}

impl WindowsApp {
    fn event_close(&self) {
        nwg::stop_thread_dispatch();
    }

    fn event_previous_track(&self) {
        let mut state = self.state.borrow_mut();
        (*state).previous_track();
    }

    fn event_play_pause(&self) {
        let mut state = self.state.borrow_mut();
        (*state).play_pause();
    }

    fn event_stop(&self) {
        let mut state = self.state.borrow_mut();
        (*state).stop();
    }

    fn event_next_track(&self) {
        let mut state = self.state.borrow_mut();
        (*state).next_track();
    }
}

pub fn init() {
    nwg::init().expect("Failed to init Native Windows GUI");
    nwg::Font::set_global_family("Segoe UI").expect("Failed to set default font");
    let state: RefCell<State> = RefCell::new(Default::default());
    let app = WindowsApp { state, ..Default::default() };
    let _ui = WindowsApp::build_ui(app).expect("Failed to build UI");

    unsafe {
        AttachConsole(ATTACH_PARENT_PROCESS);
    }

    nwg::dispatch_thread_events();
}