// extern crate embed_resource;
use embed_manifest::{embed_manifest, new_manifest};
use {
    std::io,
    winres::WindowsResource,
};

fn main() -> io::Result<()> {
    if cfg!(target_os = "windows") {
        let _ = embed_manifest(new_manifest("mixtape"));

        let mut res = WindowsResource::new();
        res.set_icon("resources/icon.ico")
            .set("ProductName", "mixtape")
            .set("InternalName", "mixtape")
            .set("CompanyName", "slippery nickels")
            .set_version_info(winres::VersionInfo::PRODUCTVERSION, 0x0000000100000000)
            .set("Comments", "i love you.");
        res.compile()?;
        embed_resource::compile("resources/mixtape.rc", embed_resource::NONE);
    }

    println!("cargo:rerun-if-changed=build.rs");

    Ok(())
}
