use crate::playlist::Playlist;

#[derive(Default)]
pub struct State {
    paused: bool,
    time: u16,
    playlist: Playlist
}

impl State {
    pub fn play_pause(&mut self) {
        self.paused = !self.paused;
    }

    pub fn next_track(&mut self) {}

    pub fn previous_track(&mut self) {}

    pub fn stop(&mut self) {
        self.paused = true;
        self.time = 0;
    }
}