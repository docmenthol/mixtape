#![allow(dead_code)]
// #![no_main]
// #![cfg_attr(not(test), no_main)]
// #![windows_subsystem = "windows"]
#![cfg_attr(not(test), windows_subsystem = "windows")]

mod state;
mod win;
mod playlist;

extern crate libc;
extern crate native_windows_derive as nwd;


/* All this goofy shit is to support tests
 * and can all be undone later.
 */

// #[no_mangle]
// pub extern "C" fn main(_argc: isize, _argv: *const *const u8) -> isize {
//     // win::init();
//     playlist::parse_playlist_file("resources/test_playlist.mix").expect("it broke.");
//     0
// }

// #[no_mangle]
// pub fn main(_argc: isize, _argv: *const *const u8) -> isize {
fn main() {
    win::init();
}